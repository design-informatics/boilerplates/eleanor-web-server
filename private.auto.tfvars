tf_vars = {
        email = "{{ email }}"     
        {%- if runner != "none" %}
        gitlab_runner_token = "",
        {%- endif -%}
        {%- if runner == "docker" or runner == "both" %}
        docker_runner_description = "{{ project_hyphen + '-runner' }}",
        docker_runner_tag_list = "production",
        {%- endif -%}
        {%- if runner == "shell" or runner == "both" %}
        shell_runner_description = "{{ project_hyphen + '-runner' }}",
        shell_runner_tag_list = "production",
        {%- endif %}
        ip_address = "",
        {%- if nginx == "static" or nginx == "both" %}
        url_static = "",
        {%- endif -%}
        {%- if nginx == "proxy" or nginx == "both" %}
        url_app = "",
        {%- endif %}
        ssh_public_key = ""
    }