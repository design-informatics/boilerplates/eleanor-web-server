# {{ project_name }}

## Prerequisites
To configure and deploy this web server on the Eleanor cloud computing service there are a few steps you need to take first.

### Install Terraform
Install Terraform by following the relevant instructions [here](https://learn.hashicorp.com/tutorials/terraform/install-cli).

### Access the Eleanor Service
1. First you need to ensure you have access to Eleanor by following instructions [here](https://www.wiki.ed.ac.uk/display/ResearchServices/Cloud+Access).
2. Now go to https://horizon.ecdf.ed.ac.uk and login with your EASE username and password. The *Domain* field should be set to `ed`.
3. Once you are logged in, you can click the dropdown to the right of the openstack logo (in the top left corner) and select the project that you'd like to use for this server.
4. Now go to the [API Access page](https://horizon.ecdf.ed.ac.uk/project/api_access/) where you should see a dropdown button to the right, labelled **Download OpenStack RC File**. Use this dropdown to download both the **OpenStack clouds.yaml File**, and **OpenStack RC File** into the same directory as this README.

### Set up IP address and domain
You need to create an IP address for your server to use. You will probably also want a domain to point to this IP address (e.g. myproject.designinformatics.org).
#### Create a floating IP address 
1. Go to the [Eleanor Floating IPs dashboard](https://horizon.ecdf.ed.ac.uk/project/floating_ips/)
2. Click the **Allocate IP To Project** button in the top right 
3. Select whether you want a public or private IP from the **Pool** dropdown.
4. Give your IP a description, then click **Allocate IP**.
5. Once the IP address has been created, add it to the `private.auto.tfvars` file under the **ip_address** variable (e.g. `ip_address = "129.215.193.9"`).

#### Map your domain(s) the the IP address
If you are serving both a static site and a web app then repeat this process for each domain.
1. Purchase a domain, or use a subdomain of an existing domain. 
2. Once you've got a domain, go to the DNS setting on the domain provider's website, and create an "A Record" pointing your chosen domain to the IP address obtained above.
3. Now add the domain to the `private.auto.tfvars` file under the **url_app** or **url_static** variable (e.g. `url_app = "myproject.designinformatics.org"`).
{% if runner != "none" %}
### Get your GitLab Runner Token
1. Log into GitLab [here](https://git.ecdf.ed.ac.uk/).
2. Navigate to the project that you want to deploy on this server.
3. Go to **Settings -> General** and under the **Visibility, project features, permissions** item, make sure the CI/CD setting is turned on. Then click **Save changes**.
4. Go to **Settings -> CI/CD** and under the **Runners** item click **New project runner** to create a new runner, taking note of the **runner token**.
5. Now add the runner token created in the previous step to the `private.auto.tfvars` file under the **gitlab_runner_token** variable (e.g. `gitlab_runner_token = "flrt-DDDRtQHJ33zkZdux_Xp_"`).
{% endif %}
### Add your SSH Key
To access to your server via ssh, you need to add your public key to the `private.auto.tfvars` file.
1. Follow instructions [here](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/checking-for-existing-ssh-keys) to find your **public** ssh key (it should end with the `.pub` extension).
2. Copy the contents of the key file to the `private.auto.tfvars` file under the **ssh_public_key** variable (e.g. `ssh_public_key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5BBBBINebBf9Yb5zaAKx+SHfXMMCBYDdP6XxwxPjm3CD0SXB6 emorgan@ed.ac.uk"`).

## Deploying the Server with Terraform
1. Open a terminal and go to the root directory for this project.
2. Initialise terraform with `terraform init`.
3. Run `terraform plan` to simulate the actions that terraform will perform. This will provide an output specifying the resources to be created and/or destroyed.
4. Terraform needs authentication credentials in your local environment in order to deploy the server resources on Eleanor. To create these, run the **OpenStack RC File** script that you downloaded earlier. For example `source ECA_DesignInformatics_sandbox-openrc.sh`.
5. Now run `terraform apply` to create the resources.

