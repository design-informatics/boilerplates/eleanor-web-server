terraform {
  required_providers {
    openstack = {
      source = "terraform-provider-openstack/openstack"
      version = "1.53.0"
    }
  }
}

provider "openstack" {
   cloud = "openstack" 
}

variable "tf_vars" {
   type = map
}

resource "openstack_compute_keypair_v2" "{{ project_hyphen + '-keypair' }}" {
  name       = "{{ project_hyphen + '-key' }}"
  public_key = var.tf_vars["ssh_public_key"]
}

resource "openstack_compute_instance_v2" "{{ project_slug }}" {
  name            = "{{ project_hyphen }}"
  image_id        = "{{ image }}"
  flavor_id       = "{{ flavour }}"
  key_pair        = "${openstack_compute_keypair_v2.{{ project_hyphen + '-keypair' }}.name}"
  security_groups = ["default"]
  user_data = templatefile("cloud-config.yml", var.tf_vars)  

  metadata = {
    this = "that"
  }

  {% if network == "public" %}
  network {
    name = "VM Network Public"
  }
  {% else %}
  network {
    name = "VM Network Private"
  }
  {% endif %}

}

resource "openstack_compute_floatingip_associate_v2" "{{ project_slug + '_ip'}}" {
   floating_ip = var.tf_vars["ip_address"]
   instance_id = openstack_compute_instance_v2.{{ project_slug }}.id
   fixed_ip    = openstack_compute_instance_v2.{{ project_slug }}.network.0.fixed_ip_v4
}



