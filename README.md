# Eleanor Web Server Boilerplate

![Example command line customisation](example.png)

## Description
This boilerplate allows you to easily create a static or dynamic web server on the University of Edinburgh's cloud computing service - [Eleanor](https://www.wiki.ed.ac.uk/display/ResearchServices/Eleanor). Using [copier](https://copier.readthedocs.io/en/stable/) you can easily create your own version of this template, customising the features below.

### Features
- Create an Eleanor virtual machine (VM) under either the Paid or Free Tiers
- Select your "flavour" of VM
- Create either a dynamic or static server (or both) using NGINX
- Choose whether your server is private or public
- Automatically generate an SSL certificate (for public servers)
- Use a GitLab runner on your server to deploy code using GitLab CI/CD

The template creates a set of files, which allow you to use [Terraform](https://www.terraform.io/) to deploy your server using a single command - `terraform apply`.

## Instructions
1. Install copier by following the instructions (here)[https://copier.readthedocs.io/en/stable/#installation].
2. Open a terminal window and use the following command to generate your customised project `copier copy https://git.ecdf.ed.ac.uk/design-informatics/boilerplates/eleanor-web-server.git path/to/destination`, where `path/to/destination` should be replaced with the path to the destination for your project. You will be prompted for your GitLab username (UUN) and password.
3. Follow the prompts to customise your project.
4. Once your project is created, see the README file in the project directory for further instructions.

